FROM ubuntu

RUN apt-get update && apt-get install -y curl && \
    curl -sL https://deb.nodesource.com/setup_13.x | bash - && \
    apt-get install -y nodejs libpoppler-qt5-dev libcairo2-dev poppler-data

VOLUME [ "/data" ]

WORKDIR /data

CMD [ "npm", "run", "start:docker" ]
