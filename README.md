# FDF

spike for editing fillable PDF file.

## Prerequirement

Make sure that these two commands are in you PATH:

- docker
- docker-compose

## How to Use

### Install dependencies

```sh
npm install
```

### Generate Filled PDF

```sh
npm start
```

The generated file is located at `/static/out.pdf`
