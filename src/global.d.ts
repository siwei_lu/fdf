interface FDFItem {
  name: string
  page: number
  value?: string | boolean
  id: number
  type: 'text' | 'checkbox' | 'combobox'
}

interface FDFData {
  [key: string]: string | boolean
}

interface FDFOptions {
  save: 'pdf' | 'pdfimg'
  cores?: number
  scale?: number
  antialias?: boolean
}

declare module 'pdf-fill-form' {
  function read(path: string): Promise<FDFItem[]>
  function write(
    path: string,
    data: FDFData,
    options: FDFOptions
  ): Promise<string>
}
