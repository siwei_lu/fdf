import pff from 'pdf-fill-form'
import path from 'path'
import fs from 'fs'

const input = path.resolve(__dirname, '..', 'static', 'i90.pdf')
const output = path.resolve(__dirname, '..', 'static', 'out.pdf')

const data = {
  65536: 'Family Name',
  65539: true,
  65548: 'AA',
  65555: '123456789',
  458756: 'AdditionalInfo AdditionalInfo AdditionalInfo AdditionalInfo AdditionalInfo AdditionalInfo AdditionalInfo AdditionalInfo AdditionalInfo AdditionalInfo',
}

async function main() {
  const result = await pff.write(input, data, { save: 'pdf' })
  fs.writeFileSync(output, result)
}

main()
